import json
from flask_login import current_user, login_required
from ..app import db, app, get_base_url
from ..models.user import User
import os
import flask
import requests
from urllib.parse import urlencode

DISCORD_FILE_PATH = "/tmp/lms42-discord.txt"

def get_redirection_url():
    return f"{get_base_url()}/discord/authorize"


def message_teachers(title: str, message: str) -> None:
    """Sends a message to all teachers on discord

    Args:
        title (str): The title of the message
        message (str): The message to send
    """    
    teachers = User.query.filter_by(is_active=True, is_hidden=False, level=50).all()
    
    for teacher in teachers:
        if teacher.discord_id is not None:
            send_dm(teacher.discord_id, title, message)

def get_auth_url():
    base_url = "https://discord.com/api/oauth2/authorize?"
    parameters_dict = {
        "redirect_uri": get_redirection_url(),
        "response_type": "code",
        "scope": "identify",
        "client_id": os.environ.get("DISCORD_CLIENT_ID")
    }
    parameters = urlencode(parameters_dict)
    return base_url + parameters


@app.route('/discord/remove', methods=['POST'])
@login_required
def discord_remove():
    user = User.query.get(current_user.id)
    user.discord_id = None
    db.session.commit()

    flask.flash("Discord account unlinked")
    return flask.redirect(f"/people/{current_user.short_name}")


@app.route('/discord/authorize')
@login_required
def discord_authorize():
    if "code" not in flask.request.args:
        flask.flash("Error in redirect url")
        return flask.redirect(f"/people/{current_user.short_name}")

    code = flask.request.args['code']
    token_url = "https://discord.com/api/oauth2/token"
    
    body = {
        "client_id": os.environ.get("DISCORD_CLIENT_ID"),
        "client_secret": os.environ.get("DISCORD_CLIENT_SECRET"),
        "grant_type": "authorization_code",
        "redirect_uri": get_redirection_url(),
        "code": code,
    }
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    
    bearer_response = requests.post(token_url, data=body, headers=headers)
    response = json.loads(bearer_response.text)
    
    if "access_token" not in response:
        flask.flash("Received error when retrieving token")
        return flask.redirect(f"/people/{current_user.short_name}")

    bearer_token = response["access_token"]

    info_url = "https://discord.com/api/oauth2/@me"
    headers = {"Authorization": f"Bearer {bearer_token}"}
    user_info = requests.get(info_url, headers=headers)
    discord_user_id = json.loads(user_info.text)["user"]["id"]
    
    user = User.query.get(current_user.id)
    user.discord_id = discord_user_id
    db.session.commit()

    if not update_nickname(user):
        flask.flash("You are currently not in the SD42 discord")

    return flask.redirect(f"/people/{current_user.short_name}")

@app.route('/discord/notifications/same_exercise', methods=['POST'])
@login_required
def discord_toggle_same_exercise_notification():
    user = User.query.get(current_user.id)
    user.discord_notification_same_exercise = bool(flask.request.form.get("notification_same_exercise", False)) # Checkbox value is either "on" or None
    db.session.commit()
    
    return flask.redirect(f"/people/{current_user.short_name}")

@app.route('/discord/notifications/exam_reviewed', methods=['POST'])
@login_required
def discord_toggle_exam_reviewed_notification():
    user = User.query.get(current_user.id)
    user.discord_notification_exam_reviewed = bool(flask.request.form.get("notification_exam_reviewed", False))
    db.session.commit()
    
    return flask.redirect(f"/people/{current_user.short_name}")

def update_nickname(user):
    server_response = bot_command("users/@me/guilds")
    discord_severs = json.loads(server_response.text)
    
    if not user.discord_id:
        return False

    success = False

    for discord_server in discord_severs:
        nickname = f'{user.first_name} {user.last_name}'
        json_data = {
            'nick': nickname
        }
    
        update_url = f'guilds/{discord_server["id"]}/members/{user.discord_id}'
        username_response = bot_command(update_url, 'PATCH', json_data)

        if username_response.status_code == 204:
            success = True

    return success


def send_dm(discord_id, title, content):
    if not os.environ.get("DISCORD_BOT_TOKEN"):
        with open(DISCORD_FILE_PATH, 'w') as file:
            file.write(content)
            return

    dm_channel = bot_command(endpoint="users/@me/channels", method="POST", json_data={"recipient_id": discord_id}).json()
    json_data = {
        "tts": False,
        "embeds": [{
            "title": title,
            "description": content
        }]
    }
    if 'id' in dm_channel.keys():# Make sure the channel id is available
        bot_command(endpoint=f"/channels/{dm_channel['id']}/messages", method="POST", json_data=json_data)


def bot_command(endpoint, method="GET", json_data=None):
    request_url = f'https://discord.com/api/{endpoint}'
    headers = {
        "Authorization": f'Bot {os.environ.get("DISCORD_BOT_TOKEN")}'
    }

    if method == "GET":
        response = requests.get(request_url, headers=headers)
    elif method == "POST":
        response = requests.post(request_url, headers=headers, json=json_data)
    elif method == "PATCH":
        response = requests.patch(request_url, headers=headers, json=json_data)
    elif method == "PUT":
        response = requests.put(request_url, headers=headers, json=json_data)
    else:
        raise ValueError(f"Unknown method: {method}")

    return response
