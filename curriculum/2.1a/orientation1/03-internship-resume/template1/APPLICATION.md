# Generic application message

Beste <name>, / Dear <name>,

Bij deze ... / I hereby apply as ...


# What info enclose?

[ ] Your CV: upload the .pdf with the assignment
[ ] LinkedIn link:
[ ] GitHub link: 
[ ] GitLab link: 
[X] Info about our study programme: https://sd42.nl/curriculum/internship-orientation/static/AdSD-stage.pdf
[ ] .......?
