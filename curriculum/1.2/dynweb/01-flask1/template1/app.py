# These should be all the imports you need:
from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

# Run using `poetry install && poetry run flask run --reload`

with open('words.txt', encoding='utf-8') as file:
    words = file.read().strip().split("\n")

def guess_to_hint(guess, secret):
    """Given a `guess` and a `secret` word as strings, it returns a list with one tuple for
    each letter in the guess, where the first item is the letter, and the second item is one
    of the strings `correct`, `wrong` or `misplaced`, describing what applies for that letter.
    """
    result = []
    for idx, letter in enumerate(guess):
        actual = secret[idx]
        if actual == letter:
            result.append((letter, 'correct'))
        elif letter in secret:
            result.append((letter, 'misplaced'))
        else:
            result.append((letter, 'wrong'))
    return result

@app.route("/", methods=['GET', 'POST'])
def index():
    """When the user visit the root of the website with a GET request and starts/resume the game,
    or when the user POSTs a new guess."""
    # TODO Maintain a list of guessed words
    # TODO When the user is POSTing a new guess, add it to the list
    # TODO Render a template, passing it the list of guessed words converted to hints (`guess_to_hint`)
    # TODO Determine if the game was lost/won, and pass this state to the template
    return "Hello web!"

@app.route("/history", methods=['GET'])
def show_history():
    """Show history of the attempts the user made."""
    return "Here is your game history"
