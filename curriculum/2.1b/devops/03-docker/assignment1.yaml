- Introduction: |
    In this chapter, you'll get familiar with the concept of "containers". You will look at what containers are, how you can use them and finally how to create containers for your own application. The objectives are structured in such a way that you'll be guided from introduction to final realization, where the need to "experiment" increases over time. Therefore it is **highly** recommended that you follow our steps and confirm that you understand each step before proceeding.

- Tasks:
    - Install docker: |
        As Docker has an official Manjaro package, you can install it using *Add/remove software*. You'll need to install the `docker` and `docker-compose` packages.

        To verify that docker has been installed correctly, run the following command:

        ```sh
        $ sudo docker version
        ```

        The first thing you will notice is that there will be an error, stating *Cannot connect to Docker daemon*: This is because the Docker Engine hasn't actually started yet.
        There should be some information about the *client*. You will use this client to instruct the engine *what to do*. The engine itself is a background process.

        Start the Docker Engine by running.
        ```sh
        $ sudo systemctl start docker.service
        ```
        Note that starting the docker service does not mean that it will automatically start after a reboot. If you want to automatically start the docker service after a reboot you should 'enable' it as follows:

        ```sh
        $ sudo systemctl enable docker.service
        ```

        In order to work with the docker service, you'll need permission to do so. Add yourself to the `docker` group using this command:

        ```sh
        $ sudo usermod -aG docker $USER
        ```

        Changes in user groups are only loaded when logging in, so at this point your should logout and log back into KDE, or reboot your computer.

        Now confirm that the engine has started by repeating the first command (`docker version`) before doing the check below.

    - Verify with "Hello Docker": |
        To verify that your installation was a success, run the "hello-world" docker image and study it's output. A docker image contains the files needed to run a particular application. Running the docker image in a docker container will execute the application stored in the image. 
        
        To launch a new container with the "hello-world" image, run:
        ```
        $ docker run hello-world
        ```

        As you will see, the required image will be downloaded automatically and a container started. The output should look something like:

        ```
        Hello from Docker!
        This message shows that your installation appears to be working correctly.
        ...
        ```

        If you see output above it means docker is correctly installed on your laptop. If not, please ask your mentor or teacher for help before you start with the assignment.


- Assignment:
    # - Introduction: 
    #     -
    #         link: https://youtu.be/JSLpG_spOBM
    #         title: Introduction To Docker and Docker Containers
    #         info: A short history on virtualization and containerization.
    #     - |
    #         Before you start, please watch the video linked to this introduction. This will give you an overview of what the Docker is. 

    #         The most *basic* usage for Docker is to use containers to host *services*. A *service* is some kind of application that might consist of one or multiple parts (e.g. some html pages (website) or a Flask backend using a postgres database).

    #         When others want to deploy your service (or as you might know it: *install it*), it requires knowledge of how you build your application. Does it require a database, what OS should be used, what applications are used to run the actual code (Java? Python? Rust? etc.), what dependencies are required, etc. etc. As you can imagine, deploying a service usually requires *effort*. The idea behind docker is to minimize this deployment effort using prepackaged containers (as explained in the video).

    - Primer:
        -
            link: https://automateinfra.com/2022/01/11/how-to-create-a-new-docker-image-using-dockerfile-dockerfile-example/
            title: How to create a new Docker image using Dockerfile
            info: Explains how to create custom Docker images with a Dockerfile
        -
            text: |
                At this point you should have docker installed and should have run your first ("Hello World") container. Before we dive deeper into Docker we want to have a look at how this "Hello World" container works. If your have not watched the [Learn Docker in 7 Easy Steps - Full Beginner's Tutorial](https://www.youtube.com/watch?v=gAkwW2tuIqE), do so now.
                
                Containers are running **Docker Images**. For our "Hello World" container we have used an image that was already created. This image can be found on [Docker Hub](https://hub.docker.com/_/hello-world/). Where did this image come from? This image was build using a `Dockerfile` and then uploaded to Docker Hub for everyone to use. The process from Dockerfile to a running container is shown below. 

                <img src="docker-process.png" class="no-border">

                The purpose of this objective it to create and run our own "Hello World" container. In the `0-primer` folder you will find a `Dockerfile` and `analysis.txt` file. Have a look at the Dockerfile and try to understand what everything means. Then open the `analysis.txt` and follow the instructions.
            ^merge: feature

    - Your first container:
        - 
            text: |
                Let's build a Docker image that can run a static website. In the template folder you will find a folder called `1-container`. It contains a simple static website that you can run using the provided script (`start.sh`). The script also logs the requests to a file called `log.txt`. Run the script, visit the website (on port 9000) and validate that the log file is created and contains the requests.
                
                Your task is to finish the Dockerfile so that:
                1. the container should have `/app` as working directory,
                2. required files are copied to the working directory, and
                3. the `start.sh` script is run on container startup using [CMD](https://docs.docker.com/engine/reference/builder/#cmd) command to.

                Build your image and run your container using docker's `-p` flag to expose container port 9000 to host port 9000. Verify that the static website is being served by opening the following url in your browser `http://localhost:9000`.
            must: true

    - Script your container management:
        - 
            link: https://www.freecodecamp.org/news/docker-detached-mode-explained/
            title: Docker detached mode
            info: The option --detach or -d, means that a Docker container runs in the background of your terminal. 
        - 
            text: |
                When you run your container from the terminal the information from the container is displayed in your terminal. This is because the container is running in the foreground. By running the container in detached mode you can move the process to the background. 

                In the template folder you will find a `todo.sh` script. Edit the script so that when you run `./todo.sh 1` it will build and run the container in detached mode and with a proper name.  
            must: true

    - Container diagnostics:
        -
            link: https://www.baeldung.com/ops/docker-container-shell
            title: Getting Into a Docker Container’s Shell
            info: A small tutorial that explains how to run an interactive shell in the container.
        - 
            text: |
                Sometimes your container does not behave like it should. In such cases it is useful to 'connect' to your container (running a terminal) and see what is happening inside. Note that the container is a basic linux environment. So most linux commands can be used. 
                
                Currently your container logs the requests to the static website. Your task is to:
                1. Shell to the container (run it if it is not running yet).
                2. Locate the `log.txt` in the container.
                3. Copy its contents (using the linux `cat` command and copy/paste) to the `1-container/container.log` file in your template folder.

                *Tip*: With `docker ps` you can see your running containers. 
            must: true

    - Run existing images:
        -
            link: https://www.youtube.com/watch?v=p2PH_YPCsis
            title: Docker Volumes explained in 6 minutes
            info: In this video the concept of Docker volumes is explained. Section after 4:14 (on docker composed is not required at this point).
        -
            link: https://youtu.be/xGn7cFR3ARU?t=608
            title: 8 Basic Docker Commands (docker ports, docker port mapping)
            info: In this video the concept of Docker ports and port mapping is explained. 
        -
            text: |
                Aside from building your own images you can also leverage existing images. For example, [nginx](https://www.nginx.com) (a frequently used web server application) is also available as a Docker image, also [see](https://hub.docker.com/_/nginx/). Give it a try by running `docker run --name my-nginx -p 8080:80 nginx`. What do you see when you open your browser on `http://localhost:8080`?

                Your task is to use serve the static website using nginx. You should:
                1. Find out where the nginx's default folder for serving static content is located.
                2. Start nginx with a *volume map* for this folder, mapping to `1-container` folder on your host machine. The *Hello Docker* page should show up in your browser.
                3. Edit the script such that running `./todo.sh 2` will start nginx with the above volume mapping such that is available on port `http://localhost:8080` of the host machine.
            must: true

    - Reverse proxy:
        - 
            link: https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
            title: NGINX Reverse Proxy
            info: Explains how to configure nginx as a reverse proxy (carefully read the section "Passing a Request to a Proxied Server")
        -
            text: |
                A reverse proxy is a server that forwards client requests to another web server and serves the response back to the client. Have a look a the provided resource to understand how this can be done using nginx.

                Your task is to configure the default nginx container as a reverse proxy. You should:
                1. Locate and study the nginx configuration file (note that the nginx.conf does not need to be edited)
                2. Create a volume mapping between the configuration file provided in the template (see `2-reverse-proxy`)
                3. Edit the `2-reverse-proxy/reverse_proxy.conf` file such that requests to [http://localhost:8080/joke](http://localhost:8080/joke) are forwarded to [https://official-joke-api.appspot.com/random_joke](https://official-joke-api.appspot.com/random_joke).
                4. Edit the script so that when you run `./todo.sh 3` it will run nginx with the above configuration and the provided static site.
            must: true

    - Docker compose:
        -
            link: https://www.youtube.com/watch?v=Qw9zlE3t8Ko
            title: Docker Compose in 12 Minutes
            info: Learn how to get multiple Docker containers to work together.
        - 
            link: https://docs.docker.com/compose/gettingstarted/
            title: Get started with Docker Compose
        -
            text: |
                Finally we have Docker compose which can be used to run multiple containers. With Docker compose you can configure which images to build and containers to run in one single file (called `docker-compose.yml`). First, make sure `docker-compose` is installed on your laptop.

                Your task is to edit the `docker-compose.yml` in the `3-compose` folder so that a standard nginx container is started with:
                1. a volume mapping to the static website (`1-container`) and
                2. a port mapping to port 8080 (making the website reachable from [http://localhost:8080](http://localhost:8080)).

                When you run `docker compose up` the website should be displayed.
            must: true 

    - Bring it all together:
        -
            text: |
                In this objective we will bring all aspects from the previous objects together into one objective. Your task will be to run a frontend and backend application in separate containers (using docker compose) as shown in the image below. 
                
                <img src="docker-custom-container-multiple.png" class="no-border">
                
                You should edit the `todo.sh` so that running `./todo.sh 4` will do everything that is required to run the application when running `docker compose up` (which should be the final command in the script). Your script should take the following requirements into account:
                
                1. For the **frontend** container:
                    - The code can be found at [https://gitlab.com/sealy/simple-todo-app](https://gitlab.com/sealy/simple-todo-app). Git clone this repository into the `4-complete/frontend` folder (from within your script).
                    - The git repository has a branch called `backend-connection-vite`, switch to this branch using `git checkout`.
                    - The frontend should be **build first** before it can be served (by nginx). When you run `npm run build` a distribution is created in a folder called `dist`. The contents of this `dist` folder can be served with an nginx container using a volume mapping. Configure this in the `docker-compose.yml` file. In your docker compose you should:
                        1. Use an off-the-shelf web server container (nginx) with the local port (80) mapped to the host computer on port 8080.
                        2. Use a volume mapping to serve a distribution (dist) folder of the frontend app.
                        3. Configured nginx to use a reverse proxy that redirects request from [http://localhost:8080/todos](http://localhost:8080/todos) to the backend container. *Note*: You should use name of the container (for example *backend*) in your docker-compose file as a hostname within your nginx reverse proxy configuration file.
                2. For your **backend** container:
                    - The backend code can be found [https://gitlab.com/sealy/simple-todo-backend](https://gitlab.com/sealy/simple-todo-backend). Git clone this repository to the `4-complete/backend` folder (from within your script).
                    - The backend is a Flask application that can be run using poetry. Edit the `Dockerfile` in `4-complete/backend` folder so that a Docker image is created with poetry installed and the required files from the backend git repository (copied) in it. 
                    - *Note*: the backend application runs on port 5000. We are using a reverse proxy to connect to the backend from within the frontend. Ask your self if a port mapping is required?
            ^merge: feature
            weight: 1.5

    - Clean up:
        -
            link: https://linuxhandbook.com/remove-docker-images/
            title: Complete Guide for Removing Docker Images
            info: Explains the various ways to remove a docker image
        -
            text: |
                Implement the clean up script (`./todo.sh 5`). It should:
                1. Stop all your containers (so other containers should not be stopped)
                2. Remove your images (please leave the other images alone)
            must: true


