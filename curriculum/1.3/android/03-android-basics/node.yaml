name: My First App
description: Create your first Android application.
allow_longer: true
goals:
    android-basics: 1
assignment:
    Install Android Studio: |
        *Android Studio* is the official Google IDE for creating Android applications. It is based on IntelliJ, so it feel somewhat familiar. Install the *AUR* package `android-studio` using *Add/Remove Software* to quickly get started.

    Getting started with Android programming:
        -
            link: https://www.educative.io/blog/how-to-develop-an-android-app
            title: "Android Tutorial: How to Develop an Android App"
            info: A good introductory text on what parts combine to make up an Android app.
        -
            link: https://www.youtube.com/watch?v=dFlPARW5IX8
            title: Android Studio For Beginners Part 1
            info: A very hands-on introduction to Android Studio and creating your first Android app.

    A dicey app:

        -
            must: true
            text: |
                You should be able to demo your app on an actual device, or on the emulator. If you have an Android device, we highly recommend using it, as it will be *way* faster than the emulator, and allows you to experience your app the way it is intended.

                - Connect your device to your laptop using USB.
                - Enable the 'developer' menu on your phone, by opening *Settings > About phone*, scrolling down to the bottom and tapping *Build number* **seven times**. You should see a message saying *You are now a developer*. Cheers to that!
                - Now you should be able to open *Settings > Developer options*, and enable *USB debugging*. Then permit your computer access.

                If you don't have an Android device you can use for this module, create an emulator device instead: Android Studio, Main Menu (the horizontal lines in the top left), Device Manager, plus-icon, and select a nice device.

            ^merge: feature
            code: 0
            text: |
                <img class="side" src="dice1.png">

                Suppose you want to play a friendly game of Monopoly. You have the board, the fake money and everything.... except dice. No worries, you'll just pull out Android Studio, create a dice rolling app, and you you'll be ready to play Monopoly in a jiffy!

                The first version of your app should look (a bit) like the screenshot. It has a TextView that displays the value of the two dice and their sum. And it has a *Roll* button, that'll set the dice values to random numbers between 1 and 6 (inclusive), updating the sum as well.

                To get started:
                
                - Within Android Studio, choose *New project* → *No Activity*.
                - As your programming language, select Java (instead of Kotlin).
                - Wait until there's no more activity in the status bar...
                - Using the menu bar, select *File* → *New Activity* → *Empty Views Activity*. Make sure you check the *Launcher Activity* option, which causes this activity (screen) to open when you start the application.
                - Press the *Run app* button (or `shift-F10`). An Android app with just an empty screen titled *MainActivity* should open on your phone or emulator.

                Good luck, go swim! (And don't worry about getting the layout exactly like the screenshot. We'll delve into layouts in the next lesson.)


        -
            link: https://www.youtube.com/watch?v=Y7JTkXoN8OE
            title: How to add images to Android Studio
            info: A tutorial on how to display images in an Android app.

        -
            ^merge: feature
            code: 0
            text: |
                <img class="side" src="dice2.png">

                After an hour of playing Monopoly (so with about seven hours to go), you're taking a break and pull out Android Studio again. Two additional features would really improve your evening:

                - A graphical representation of the dice. This would make your app feel a lot more *real*. The images for this have been provided in the template. They are Android vector image resources, that can be dragged into `res/drawable`.
                - And of course, a way to cheat! Tapping on one of the dice images, should increment its value by 1 (wrapping back to 1 after 6).

        -
            text: |
                Make sure your Java code is easy to understand and contains little duplication. A few hints:

                - Maintain state (the values of the two dice) and references to Android Views as class variables.
                - Create `update()` and `getDiceResource(number: Int)` methods.

            ^merge: codequality
            weight: 0.5
